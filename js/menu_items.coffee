window.menuItems = [
  {
    type: 'boolean'
    value: true
    className: 'MenuItemRecording'
  }
  {
    type: 'list'
    value: 2
    options: ['OFF','5','15','30','45','60','75','90','105','120','180','240']
    className: 'MenuItemSleepTimer'
  }
  {
    type: 'boolean'
    value: true
    className: 'MenuItemFavoriteChannel'
  }
  {
    type: 'boolean'
    value: true
    secured: true
    className: 'MenuItemParentalControl'
  }
  {
    type: 'list'
    value: 0
    options:['English', 'Spanish', 'France']
    className: 'MenuItemLanguage'
  }
]