App = Em.Application.create()

App.MenuItem = Em.Mixin.create({
  subitemWidth: 100
  xTranslate: 100
  activateSubMenuTransition: true

  activeValue: (->
    if @get('type') == 'boolean'
      @get('value')
    else if @get('type') == 'list'
      @get('options')[@get('value')]
  ).property('options', 'value')

  firstOption: (->
    if @get('options')
      @get('options')[0]
  ).property('options')

  lastOption: (->
    if @get('options')
      @get('options')[@get('options').length - 1]
  ).property('options')


  subitemsStyle: (->
    xTranslate = (-1 * @get('value') * @get('subitemWidth')) - @get('xTranslate')
    if @get('xTranslate') != 100
      @set('xTranslate', 100)
    "transform: translate(#{xTranslate}px, 0);" +
    "-webkit-transform: translate(#{xTranslate}px, 0);" +
    "-ms-transform: translate(#{xTranslate}px, 0);"
  ).property('value', 'xTranslate')

  nextValue: ->
    if @get('type') == 'boolean'
      @set 'value', !@get('value')
    else if @get('type') == 'list'
      nextValue = (@get('value') + 1) % @get('options').length

      if (@get('value') + 1 == @get('options').length)
        @set 'xTranslate', 200

        Ember.run.later =>
          @set 'activateSubMenuTransition', false
          @set 'value', nextValue
          Ember.run.later =>
            @set 'activateSubMenuTransition', true
          , 30
        , 70
      else
        @set 'value', nextValue

  prevValue: ->
    if @get('type') == 'boolean'
      @set 'value', !@get('value')
    else if @get('type') == 'list'
      prevValue = (@get('value') - 1 + @get('options').length) % @get('options').length

      if (@get('value') == 0)
        @set 'xTranslate', 0

        Ember.run.later =>
          @set 'activateSubMenuTransition', false
          @set 'value', prevValue
          Ember.run.later =>
            @set 'activateSubMenuTransition', true
          , 30
        , 70
      else
        @set 'value', prevValue
})


App.MenuItemRecording = Em.Object.extend App.MenuItem,
  title: (->
    if @get 'value'
      "Stop recording #{@get('chanel.callSign')} #{@get('chanel.number')} now"
    else
      "Start recording #{@get('chanel.callSign')} #{@get('chanel.number')} now"
  ).property('value')

  activeValue: null

App.MenuItemSleepTimer = Em.Object.extend App.MenuItem,
  title: 'Sleep Timer'

App.MenuItemFavoriteChannel = Em.Object.extend App.MenuItem,
  title: (->
    if @get 'value'
      "Remove #{@get('chanel.callSign')} #{@get('chanel.number')} from Favorites List"
    else
      "Add #{@get('chanel.callSign')} #{@get('chanel.number')} to Favorites List"
  ).property('value')

  activeValue: null

App.MenuItemParentalControl = Em.Object.extend App.MenuItem,
  title: (->
    if @get('value')
      "Turn OFF Parental Control"
    else
      "Turn ON Parental Control"
  ).property('value')

  activeValue: null

App.MenuItemLanguage = Em.Object.extend App.MenuItem,
  title: 'Language'



App.ApplicationRoute = Em.Route.extend
  setupController: (controller, model)->

    chanel = Em.Object.create
      callSign: 'CNN'
      number:   95

    items = window.menuItems
    itemsLength = items.length

    items = items[0..itemsLength - 1].map (item, i) ->
      item['chanel'] = chanel
      App[item.className].create(item)

    if localStorage.getItem('menuItems') != null
      lsMenuItems = JSON.parse localStorage.getItem 'menuItems'
      console.log lsMenuItems
      items.map (item, i) ->
        items[i].set 'value', lsMenuItems[i].value

    items = items.concat(items, items)
    items[0].set('active', true)

    itemHeight = 45
    upperItems = 2
    correction = -1 * (itemsLength - upperItems) * itemHeight

    controller.setProperties
      model:        items
      activeIndex:  0
      itemsLength:  itemsLength
      itemHeight:   itemHeight
      upperItems:   upperItems
      activateMenuTransition: true

      correction: correction
      yTranslate: correction

      chanel: chanel
      visible: false
      showSettingsTimeout: null


App.ApplicationController = Em.ArrayController.extend

  ulStyle: (->

    yTranslate = -1 * @get('activeIndex') * @get('itemHeight') + @get('yTranslate')

    if @get('yTranslate') != @get('correction')
      @set('yTranslate', @get('correction'))

    "transform: translate(0, #{yTranslate}px);" +
    "-webkit-transform: translate(0, #{yTranslate}px);" +
    "-ms-transform: translate(0, #{yTranslate}px);"
  ).property('activeIndex', 'yTranslate')

  nextActiveIndex: ->
    nextActiveIndex = (@get('activeIndex') + 1) % @get('itemsLength')

    @get('model')[@get('activeIndex')].set('active', false)
    @get('model')[nextActiveIndex].set('active', true)

    if (@get('activeIndex') + 1 == @get('itemsLength'))
      @set 'yTranslate', @get('correction') - @get('itemHeight')

      Ember.run.later =>
        @set 'activateMenuTransition', false
        @set 'activeIndex', nextActiveIndex
        Ember.run.later =>
          @set 'activateMenuTransition', true
        , 30
      , 70
    else
      @set 'activeIndex', nextActiveIndex

  prevActiveIndex: ->
    prevActiveIndex = (@get('activeIndex') - 1 + @get('itemsLength')) % @get('itemsLength')

    @get('model')[@get('activeIndex')].set('active', false)
    @get('model')[prevActiveIndex].set('active', true)

    if (@get('activeIndex') == 0)
      @set 'yTranslate', @get('correction') + @get('itemHeight')

      Ember.run.later =>
        @set 'activateMenuTransition', false
        @set 'activeIndex', prevActiveIndex
        Ember.run.later =>
          @set 'activateMenuTransition', true
        , 30
      , 70
    else
      @set 'activeIndex', prevActiveIndex


  activeItem: (->
    @get('model')[@get('activeIndex')]
  ).property('model', 'activeIndex')


  showSettings: ->
    @setShowSettingsTimeout()
    @set 'visible', true

  setShowSettingsTimeout: ->
    clearTimeout @get('showSettingsTimeout')
    @set('showSettingsTimeout', setTimeout =>
      @hideSettings()
    , 20000)

  hideSettings: () ->
    clearTimeout @get('showSettingsTimeout')
    @set 'visible', false

  updateLocaleStorage: () ->
    localStorage.clear()
    localStorage.setItem 'menuItems', JSON.stringify @get('model')

  actions:
    bottom: ->
      if @get 'visible'
        @nextActiveIndex()
        @setShowSettingsTimeout()
        @updateLocaleStorage()

    top: ->
      if @get 'visible'
        @prevActiveIndex()
        @setShowSettingsTimeout()
        @updateLocaleStorage()

    left: ->
      if @get 'visible'
        @get('activeItem').prevValue()
        @setShowSettingsTimeout()
        @updateLocaleStorage()

    right: ->
      if @get 'visible'
        @get('activeItem').nextValue()
        @setShowSettingsTimeout()
        @updateLocaleStorage()

    settings: ->
      if @get 'visible'
        @hideSettings()
      else
        @showSettings()

    ok: ->
      console.log 'ok'
      @updateLocaleStorage()

    esc: ->
      console.log 'esc'
      @updateLocaleStorage()


App.ApplicationView = Em.View.extend
  didInsertElement: ->
    $(document).on('keyup', { _self: @ }, @listentToBtns)

  willDestroyElement: ->
    $(document).off "keyup", @listentToBtns

  listentToBtns: (e) ->
    keyCode = e.keyCode || e.which

    switch keyCode
      when 13 then e.data._self.get('controller').send('ok')
      when 27 then e.data._self.get('controller').send('esc')
      when 37 then e.data._self.get('controller').send('left')
      when 38 then e.data._self.get('controller').send('top')
      when 39 then e.data._self.get('controller').send('right')
      when 40 then e.data._self.get('controller').send('bottom')
      when 83 then e.data._self.get('controller').send('settings')


App.ApplicationAdapter = DS.LSAdapter

App.MenuStatus = DS.Model.extend
  type        : DS.attr()
  value       : DS.attr()
  options     : DS.attr()
  className   : DS.attr()
  secured     : DS.attr()
